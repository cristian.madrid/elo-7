import 'package:meta/meta.dart';
import 'package:intl/intl.dart';

final NumberFormat _currencyFormatter = NumberFormat.simpleCurrency();
String formatCurrency(int value) => _currencyFormatter.format(value / 100);

class ProductOutput {
  const ProductOutput({
    @required this.title,
    @required this.id,
    @required this.link,
    @required this.picture,
    @required this.currentAmount,
    this.promotionalAmount,
    this.installmentAmount,
  })  : assert(title != null),
        assert(id != null),
        assert(currentAmount != null),
        assert(link != null),
        assert(picture != null);

  final String title;
  final String id;
  final String link;
  final String picture;
  final String currentAmount;
  final String promotionalAmount;
  final String installmentAmount;

  factory ProductOutput.fromJson(Map<String, dynamic> json) => ProductOutput(
        title: json['title'],
        id: json['id'],
        link: json['_link'],
        picture: json['picture'],
        currentAmount: formatCurrency(json['currentAmount']),
        installmentAmount: formatCurrency(json['installmentAmount']),
        promotionalAmount: formatCurrency(json['promotionalAmount']),
      );

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ProductOutput &&
          runtimeType == other.runtimeType &&
          title == other.title &&
          id == other.id &&
          currentAmount == other.currentAmount &&
          installmentAmount == other.installmentAmount &&
          promotionalAmount == other.promotionalAmount &&
          link == other.link &&
          picture == other.picture;

  @override
  int get hashCode =>
      title.hashCode ^
      id.hashCode ^
      link.hashCode ^
      currentAmount.hashCode ^
      installmentAmount.hashCode ^
      promotionalAmount.hashCode ^
      picture.hashCode;
}
